#!/usr/bin/env python

# This file is part of FS22_Vehicle_Years.
#
# FS22_Vehicle_Years is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# FS22_Vehicle_Years is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# FS22_Vehicle_Years. If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path
from typing import Final
from zipfile import ZipFile

import argparse
import xml.etree.ElementTree as ET

CDATA_START_MARKER: Final[str] = "$CDATA_START_MARKER"
CDATA_END_MARKER: Final[str] = "$CDATA_END_MARKER"
CDATA_START_REPLACE: Final[str] = "\n\t\t\t<![CDATA["
CDATA_END_REPLACE: Final[str] = "\t\t\t]]>\n\t\t"


def parse_bundle_file(bundle_file: Path) -> list[str]:
    """
    Parses an text file containing a list of mod names and information and returns a list of the
    mods in it.

    Keyword arguments:
    bundle_file (Path) -- The file to parse
    """
    result: list[str] = []

    with bundle_file.open() as f:
        lines = f.readlines()

    for line in lines:
        mod_name = line.split()[0]

        result.append(mod_name)

    return result


def generate_modDesc(start_year: int, end_year: int, mods: list[str], version: str, icon_name: str) -> str:

    root_elem = ET.Element("modDesc", descVersion="80")

    author_elem = ET.SubElement(root_elem, "author")
    author_elem.text = "Thalley"

    version_elem = ET.SubElement(root_elem, "version")
    version_elem.text = version

    iconFilename_elem = ET.SubElement(root_elem, "iconFilename")
    iconFilename_elem.text = icon_name

    title_elem = ET.SubElement(root_elem, "title")
    en_title_elem = ET.SubElement(title_elem, "en")
    de_title_elem = ET.SubElement(title_elem, "de")

    if start_year < 1950:
        en_title_elem.text = "Vehicle Bundle: Pre-1950"
        de_title_elem.text = "Fahrzeugpaket: Vor 1950"
    else:
        en_title_elem.text = f"Vehicle Bundle: {start_year}s"
        de_title_elem.text = f"Fahrzeugpaket: {start_year}s"

    description_elem = ET.SubElement(root_elem, "description")
    en_description_elem = ET.SubElement(description_elem, "en")
    de_description_elem = ET.SubElement(description_elem, "de")

    if start_year < 1950:
        en_range_text = "before 1950"
        de_range_text = "der Zeit vor 1950"
    else:
        en_range_text = f"between {start_year} and {end_year} (inclusive)"
        de_range_text = f"den Jahren zwischen {start_year} und {end_year} (einschließlich)"

    en_description_elem.text = f"""{CDATA_START_MARKER}
This mod is a bundle of modded vehicles and attachments from {en_range_text}.
The collection is based on data from FS22_Vehicle_Years and contains the highest rated vehicles from that era.

The screenshots only show a good selection of the available vehicles and attachments, as including all of them would be too many.

Disclaimer: I am not the author of any of these mods, and credit for the mods referred by this mod should go to the original authors.

Please report or correct any incorrect or missing years to the project on Gitlab: https://gitlab.com/thalley/fs22_vehicle_bundles.
{CDATA_END_MARKER}"""

    de_description_elem.text = f"""{CDATA_START_MARKER}
Dieser Mod ist ein Paket modifizierter Fahrzeuge und Anbauteile aus {de_range_text}.
Die Sammlung basiert auf Daten von FS22_Vehicle_Years und enthält die am höchsten bewerteten Fahrzeuge aus dieser Zeit.

Die Screenshots zeigen nur eine gute Auswahl der verfügbaren Fahrzeuge und Anbaugeräte, da die Auflistung aller zu viel wäre.

Haftungsausschluss: Ich bin nicht der Autor dieser Mods und die Anerkennung für die von diesem Mod verwiesenen Mods sollte den ursprünglichen Autoren zustehen.

Bitte melden oder korrigieren Sie falsche oder fehlende Jahresangaben für das Projekt auf Gitlab: https://gitlab.com/thalley/fs22_vehicle_bundles.
{CDATA_END_MARKER}"""

    multiplayer_elem = ET.SubElement(root_elem, "multiplayer", supported="true")

    dependencies_elem = ET.SubElement(root_elem, "dependencies")
    for mod in mods:
        dependency_elem = ET.SubElement(dependencies_elem, "dependency")
        dependency_elem.text = mod

    elem_tree = ET.ElementTree(root_elem)
    ET.indent(elem_tree, space="\t")

    # Since xml.etree.ElementTree does not support CDATA we simply convert the XML to a string and
    # store it ourselves, all while replacing our marker with proper CDATA tags
    xml_str = ET.tostring(root_elem, encoding='utf-8').decode('utf-8')
    xml_str = """<?xml version="1.0" encoding="utf-8" standalone="no" ?>\n""" + xml_str
    xml_str = xml_str.replace(CDATA_START_MARKER, CDATA_START_REPLACE)
    xml_str = xml_str.replace(CDATA_END_MARKER, CDATA_END_REPLACE)

    return xml_str


def get_icon_name_from_year(start_year: int):
    if start_year < 1950:
        icon_name = "icon_vbp1950.dds"
    else:
        icon_name = f"icon_vb{start_year}s.dds"

    return icon_name


def update_bundle_mod(bundle_data: Path, bundle_dir: Path, start_year: int, end_year: int, version: str):
    icon_name = get_icon_name_from_year(start_year)

    if start_year < 1950:
        bundle_data_filename = "pre_1950.txt"
    else:
        bundle_data_filename = f"{start_year}s.txt"

    mods = parse_bundle_file(bundle_data.joinpath(bundle_data_filename))
    modDesc = generate_modDesc(start_year, end_year, mods, version, icon_name)

    if not bundle_dir.is_dir():
        bundle_dir.mkdir()

    modDesc_path = bundle_dir.joinpath("modDesc.xml")
    with modDesc_path.open("w") as f:
        f.write(modDesc)

    screenshot_dir = bundle_dir.joinpath("screenshots")
    if not screenshot_dir.is_dir():
        screenshot_dir.mkdir()


def pack_mod(bundle_dir: Path, mod_zip: str, icon_name: str):
    with ZipFile(mod_zip.absolute(), "w") as z:
        z.write(bundle_dir.joinpath("modDesc.xml").absolute(), arcname="modDesc.xml")
        z.write(bundle_dir.joinpath(icon_name).absolute(), arcname=icon_name)

    print("Mod saved as " + str(mod_zip.absolute()))


def update_bundle_mods(bundle_data: Path, output_dir: Path = None):
    BUNDLES: Final[list[(str, int, int, str)]] = [
        ("pre_1950", 0, 1949, "1.0.0.2"),
        ("1950s", 1950, 1959, "1.0.0.2"),
        ("1960s", 1960, 1969, "1.0.0.2"),
        ("1970s", 1970, 1979, "1.0.0.2"),
        ("1980s", 1980, 1989, "1.0.0.2"),
        ("1990s", 1990, 1999, "1.0.0.2"),
        ("2000s", 2000, 2009, "1.0.0.2"),
        ("2010s", 2010, 2019, "1.0.0.2"),
        ("2020s", 2020, 2030, "1.0.0.2"),
    ]

    for name, start, end, version in BUNDLES:
        bundle_dir_name = f"fs22_vehicle_bundle_{name}"
        bundle_dir = Path(__file__).parent.joinpath(bundle_dir_name)

        update_bundle_mod(bundle_data, bundle_dir, start, end, version)

        if output_dir is not None:
            output_dir.mkdir(parents=True, exist_ok=True)
            mod_name = f"FS22_Vehicle_Bundle_{name}.zip"
            mod_zip = output_dir.joinpath(mod_name)
            icon_name = get_icon_name_from_year(start)

            pack_mod(bundle_dir, mod_zip, icon_name)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Arguments for the update bundles")

    parser.add_argument(
        "bundle_input_dir",
        type=str,
        help="The directory where the bundle data is stored"
    )

    parser.add_argument(
        "--mod_output_dir",
        required=False,
        type=str,
        help="The directory where the resulting packed mods are stored"
    )

    args = parser.parse_args()

    bundle_input_dir = Path(args.bundle_input_dir)
    if args.mod_output_dir is None:
        update_bundle_mods(bundle_input_dir)
    else:
        update_bundle_mods(bundle_input_dir, Path(args.mod_output_dir))
