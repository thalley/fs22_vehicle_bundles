## Description
This repo contains several mods for Farming Simulator 22 that are vehicle bundles for each decade.
The data is based on data from [FS22_Vehicle_Years](https://gitlab.com/thalley/fs22_vehicle_years).

Running `update_bundles.py` require the data from
[FS22_Vehicle_Years](https://gitlab.com/thalley/fs22_vehicle_years) and will update and optionally
pack the mods with the most recent data.


## Mod bundles only found here
Unfortunately GIANTS Software have rejected these bundle mods for Modhub, and thus they are only available here on gitlab. The suggested use of them is to download the releases at https://gitlab.com/thalley/fs22_vehicle_bundles/-/releases/, install them in your mods folder and then enable them in your game to trigger the download and enabling. After that they can safely be removed.
